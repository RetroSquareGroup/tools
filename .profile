export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
source ~/.bash-git-prompt/gitprompt.sh
PATH="$PATH:$HOME/.yarn-global/bin"
PATH="$PATH:$HOME/.gem/ruby/2.3.0/bin"
alias ag='ag --path-to-agignore ~/.agignore'
