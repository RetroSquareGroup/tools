" Use Vim settings, rather then Vi settings (much better!).
" This must be first, because it changes other options as a side effect.
set nocompatible

" ================ General Config ====================

set number                      "Line numbers are good
set backspace=indent,eol,start  "Allow backspace in insert mode
set history=1000                "Store lots of :cmdline history
set showcmd                     "Show incomplete cmds down the bottom
set showmode                    "Show current mode down the bottom
set visualbell                  "No sounds
set autoread                    "Reload files changed outside vim
set timeoutlen=1000 ttimeoutlen=0
set t_ut=

" This makes vim act like all other editors, buffers can
" exist in the background without being in a window.
" http://items.sjbach.com/319/configuring-vim-right
set hidden
set exrc

"turn on syntax highlighting
syntax on

" Change leader to a comma because the backslash is too far away
" That means all \\x commands turn into ,x
" The mapleader has to be set before vundle starts loading all
" the plugins.
let mapleader=","
let maplocalleader=","

" ================ Turn Off Swap Files ==============

set noswapfile
set nobackup
set nowb
set backupcopy=yes

" ================ Persistent Undo ==================
" Keep undo history across sessions, by storing in file.
" Only works all the time.
if has('persistent_undo')
  silent !mkdir ~/.config/nvim/backups > /dev/null 2>&1
  set undodir=~/.config/nvim/backups
  set undofile
endif

" ================ Indentation ======================

set autoindent
set smartindent
set smarttab
set shiftwidth=2
set softtabstop=2
set tabstop=2
set expandtab

filetype plugin on
filetype indent on

" Display tabs and trailing spaces visually
set list listchars=tab:\ \ ,trail:·

set nowrap       "Don't wrap lines
set linebreak    "Wrap lines at convenient points

" ================ Completion =======================

set wildmode=list:longest
set wildmenu                "enable ctrl-n and ctrl-p to scroll thru matches
set wildignore=*.o,*.obj,*~ "stuff to ignore when tab completing
set wildignore+=*vim/backups*
set wildignore+=*sass-cache*
set wildignore+=*DS_Store*
set wildignore+=vendor/rails/**
set wildignore+=vendor/cache/**
set wildignore+=*.gem
set wildignore+=log/**
set wildignore+=tmp/**
set wildignore+=*.png,*.jpg,*.gif

"
" ================ Scrolling ========================

set scrolloff=8        "Start scrolling when we're 8 lines away from margins
set sidescrolloff=15
set sidescroll=1

" ================ Search ===========================

set incsearch       " Find the next match as we type the search
set hlsearch        " Highlight searches by default
set ignorecase      " Ignore case when searching...
set smartcase       " ...unless we type a capital

" ================ Plugins =========================

call plug#begin('~/.config/nvim/plugged')

Plug 'https://github.com/tpope/vim-fugitive.git'
Plug 'https://github.com/airblade/vim-gitgutter.git'
Plug 'https://github.com/vim-airline/vim-airline.git'
Plug 'https://github.com/vim-airline/vim-airline-themes.git'
Plug 'https://github.com/pangloss/vim-javascript.git'
Plug 'https://github.com/honza/vim-snippets.git'
Plug 'https://github.com/w0rp/ale.git'
Plug 'https://github.com/tpope/vim-obsession.git'

Plug 'neovim/nvim-lspconfig'
Plug 'williamboman/nvim-lsp-installer'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/cmp-cmdline'
Plug 'hrsh7th/nvim-cmp'

Plug 'hrsh7th/cmp-vsnip'
Plug 'hrsh7th/vim-vsnip'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'bhurlow/vim-parinfer'
Plug 'hashivim/vim-terraform'
Plug 'thosakwe/vim-flutter'
Plug 'dart-lang/dart-vim-plugin'

Plug 'guns/vim-sexp',  {'for': 'clojure'}
Plug 'Olical/conjure', { 'tag': 'v4.28.0' }

call plug#end()

set completeopt=menu,menuone,noselect

lua <<EOF
  -- Setup nvim-cmp.
  local cmp = require'cmp'

  cmp.setup({
    snippet = {
      -- REQUIRED - you must specify a snippet engine
      expand = function(args)
        vim.fn["vsnip#anonymous"](args.body) -- For `vsnip` users.
        -- require('luasnip').lsp_expand(args.body) -- For `luasnip` users.
        -- require('snippy').expand_snippet(args.body) -- For `snippy` users.
        -- vim.fn["UltiSnips#Anon"](args.body) -- For `ultisnips` users.
      end,
    },
    window = {
      completion = cmp.config.window.bordered(),
      documentation = cmp.config.window.bordered(),
    },
    mapping = cmp.mapping.preset.insert({
      ['<C-b>'] = cmp.mapping.scroll_docs(-4),
      ['<C-f>'] = cmp.mapping.scroll_docs(4),
      ['<C-Space>'] = cmp.mapping.complete(),
      ['<C-e>'] = cmp.mapping.abort(),
      ['<CR>'] = cmp.mapping.confirm({ select = false }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
    }),
    sources = cmp.config.sources({
      { name = 'nvim_lsp' },
      { name = 'vsnip' }, -- For vsnip users.
      -- { name = 'luasnip' }, -- For luasnip users.
      -- { name = 'ultisnips' }, -- For ultisnips users.
      -- { name = 'snippy' }, -- For snippy users.
    }, {
      { name = 'buffer' },
    })
  })

  -- Set configuration for specific filetype.
  cmp.setup.filetype('gitcommit', {
    sources = cmp.config.sources({
      { name = 'cmp_git' }, -- You can specify the `cmp_git` source if you were installed it.
    }, {
      { name = 'buffer' },
    })
  })

  -- Use buffer source for `/` (if you enabled `native_menu`, this won't work anymore).
  cmp.setup.cmdline('/', {
    mapping = cmp.mapping.preset.cmdline(),
    sources = {
      { name = 'buffer' }
    }
  })

  -- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
  cmp.setup.cmdline(':', {
    mapping = cmp.mapping.preset.cmdline(),
    sources = cmp.config.sources({
      { name = 'path' }
    }, {
      { name = 'cmdline' }
    })
  })

  -- Setup lspconfig.
  local capabilities = require('cmp_nvim_lsp').update_capabilities(vim.lsp.protocol.make_client_capabilities())
  capabilities.textDocument.completion.completionItem.snippetSupport = true

  local lsp_installer = require("nvim-lsp-installer")

  lsp_installer.settings({
      ui = {
          icons = {
              server_installed = "✓",
              server_pending = "➜",
              server_uninstalled = "✗"
          }
      }
  })

  -- Register a handler that will be called for each installed server when it's ready (i.e. when installation is finished
  -- or if the server is already installed).
  lsp_installer.on_server_ready(function(server)
      local opts = {}

      -- (optional) Customize the options passed to the server
      -- if server.name == "tsserver" then
      --     opts.root_dir = function() ... end
      -- end

      -- This setup() function will take the provided server configuration and decorate it with the necessary properties
      -- before passing it onwards to lspconfig.
      -- Refer to https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md
      server:setup(opts)
  end)
EOF

" ================ My Stuff =========================

if has('nvim')
  let $GIT_EDITOR = 'nvr -cc split --remote-wait'
endif

let g:ctrlp_map = '<C-f>'
"let g:loaded_matchparen=1
let g:ctrlp_follow_symlinks = 1
let g:ctrlp_custom_ignore = 'node_modules\|DS_Store\|git'
let g:rehash256 = 1
let g:javascript_plugin_jsdoc = 1
let g:javascript_plugin_ngdoc = 1
let g:javascript_plugin_flow = 1
let g:jsx_ext_required = 0
let g:netrw_banner = 0
let g:netrw_liststyle = 4
let g:netrw_altv = 1
let g:netrw_winsize = 25
let g:ale_set_quickfix = 1
let g:ale_linters = {
\  'javascript': ['eslint'],
\}
let g:ale_fixers = {}
let g:ale_fixers['javascript'] = ['prettier','eslint']
let g:ale_fixers['html'] = ['prettier']
let g:ale_fixers['php'] = ['prettier']
let g:ale_fixers['css'] = ['prettier']
let g:ale_fixers['md'] = ['prettier']
let g:ale_fixers['dart'] = ['dart-format']
let g:ale_fix_on_save = 1
let g:ale_javascript_prettier_options = '--single-quote'
let g:snipMate = { 'snippet_version' : 1 }
let g:airline#extensions#tabline#enabled = 1
let g:airline_theme='minimalist'
"let g:iced_enable_default_key_mappings = v:true
let g:sexp_enable_insert_mode_mappings = 0
"let g:iced_multi_session#does_switch_session = v:true

set laststatus=2
set splitright
set splitbelow

set termguicolors
"let ayucolor="mirage"
"colorscheme ayu
colorscheme molokai

filetype plugin indent on

command! NoPretty let g:ale_fixers['javascript'] = []
command! MoarPretty let g:ale_fixers['javascript'] = ['prettier']

au BufNewFile,BufRead *.ejs set filetype=html

autocmd BufWritePre * :%s/\s\+$//e
autocmd FileType js :setlocal sw=2 ts=4 sts=2
autocmd FileType html :setlocal sw=2 ts=2 sts=2
autocmd FileType gitcommit,gitrebase,gitconfig set bufhidden=delete
autocmd TermOpen * setlocal nonumber norelativenumber

set secure

nmap <silent> \`<Up> :wincmd k<CR>
nmap <silent> \`<Down> :wincmd j<CR>
nmap <silent> \`<Left> :wincmd h<CR>
nmap <silent> \`<Right> :wincmd l<CR>
nnoremap <C-Insert> :tabnew<CR>
nnoremap <C-Delete> :tabclose<CR>
nnoremap <C-p> :tabprev<CR>
nnoremap <C-n> :tabnext<CR>
tnoremap <Esc> <C-\><C-n>
