#!/bin/bash

reldir=`dirname $0`
cd $reldir
absdir=`pwd`

sudo add-apt-repository ppa:neovim-ppa/unstable
sudo apt update
sudo apt install -y neovim python3-pip
pip3 install neovim-remote

cd ~
git clone https://github.com/magicmonty/bash-git-prompt.git .bash-git-prompt &> /dev/null
source ~/.bash-git-prompt/gitprompt.sh

echo "Setting up directories..."
mkdir -p ~/.config/nvim/autoload ~/.config/nvim/bundle ~/.config/nvim/colors ~/.config/nvim/syntax ~/.config/nvim/plugged
cd ~/.config/nvim

echo "Downloading and installing plug.vim..."
curl -LSso ~/.config/nvim/autoload/plug.vim https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim

echo "Downloading and installing Twilight theme..."
curl -LSso ~/.config/nvim/colors/twilight256.vim http://www.vim.org/scripts/download_script.php?src_id=14937

echo "Downloading and installing Molokai theme..."
curl -LSso ~/.config/nvim/colors/molokai.vim https://raw.githubusercontent.com/tomasr/molokai/master/colors/molokai.vim

echo "Downloading and installing Ayu theme..."
curl -LSso ~/.config/nvim/colors/ayu.vim https://raw.githubusercontent.com/RetroSquareGroup/ayu-vim/master/colors/ayu.vim

echo "Setting up yarn..."
mkdir ~/.yarn-global &> /dev/null
yarn config set prefix ~/.yarn-global &> /dev/null

# setup my LSPs
npm install -g typescript typescript-language-server vscode-langservers-extracted
sudo bash < <(curl -s https://raw.githubusercontent.com/clojure-lsp/clojure-lsp/master/install)

echo "ADD THIS TO YOUR .bashrc: source ~/.bash-git-prompt/gitprompt.sh"
echo "ADD THIS TO YOUR .bashrc: export PATH=\"\$PATH:\$HOME/.local/bin\""
echo "ADD THIS TO YOUR .bashrc: export PATH=\"\$PATH:`yarn global bin`\""

# if [ -n "$NVIM_LISTEN_ADDRESS" ]; then
#   export MY_VIM="nvr -cc tabedit --remote-wait +'set bufhidden=wipe'"
# else
#   export MY_VIM="nvim"
# fi
# alias vim="$MY_VIM"
# alias vi="$MY_VIM"
