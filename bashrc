source ~/.bash-git-prompt/gitprompt.sh

if [ -n "$NVIM_LISTEN_ADDRESS" ]; then
  export MY_VIM="nvr -cc tabedit --remote-wait +'set bufhidden=wipe'"
else
  export MY_VIM="nvim"
fi
alias vim="$MY_VIM"
alias vi="$MY_VIM"

