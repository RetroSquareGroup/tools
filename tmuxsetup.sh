#!/bin/bash
echo "We'll need sudo access."
sudo apt update
sudo apt install -y tmux ruby

echo "if [ ! -S ~/.ssh/ssh_auth_sock ] && [ -S \"\$SSH_AUTH_SOCK\" ]; then
    ln -sf \$SSH_AUTH_SOCK ~/.ssh/ssh_auth_sock
fi" > ~/.ssh/rc

cd /var/lib
sudo chmod -R a+w gems/
cd /usr/local
sudo chmod -R a+w bin/

gem install teamocil
mkdir ~/.teamocil
